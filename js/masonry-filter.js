;(function ($) {
  'use strict'

  $.fn.multipleFilterMasonry = function (options) {
    let cache = []
    let filters = []

    if (options.selectorType === 'list') {
      $(options.filtersGroupSelector)
        .children()
        .each(function () {
          filters.push($(this).data('filter'))
        })
    }

    let init = function ($container) {
      $container.find(options.itemSelector).each(function () {
        cache.push($(this))
      })
      $container.masonry(options)
    }

    let filterItems = function (selector) {
      let result = []
      $(cache).each(function (item) {
        $(selector).each(function (index, sel) {
          if (cache[item].is(sel)) {
            if ($.inArray(cache[item], result) === -1) result.push(cache[item])
          }
        })
      })
      return result
    }

    let reload = function ($container, items) {
      $container.empty()
      $(items).each(function () {
        $($container).append($(this))
      })
      $container.masonry('reloadItems')
      $container.masonry()
    }

    // filter
    let hashFilter = function ($container) {
      let hash = window.location.hash.replace('#', '')
      if ($.inArray(hash, filters) !== -1) {
        reload($container, $('.' + hash))
      }
    }

    let proc = function ($container) {
      $(options.filtersGroupSelector)
        .find('input[type=radio]')
        .each(function () {
          $(this).change(function () {
            let selector = []
            $(options.filtersGroupSelector)
              .find('input[type=radio]')
              .each(function () {
                if ($(this).is(':checked')) {
                  selector.push('.' + $(this).val())
                }
              })
            let items = cache
            if (selector.length > 0) {
              items = filterItems(selector)
            }
            reload($container, items)
          })
        })
    }

    let procUL = function ($container) {
      $(options.filtersGroupSelector)
        .children()
        .each(function () {
          $(this).on('click', function () {
            $(options.filtersGroupSelector).children().removeClass('selected')
            window.location.hash = $(this).data('filter')
            let selector = []
            selector.push('.' + $(this).data('filter'))
            $(this).addClass('selected')
            let items = cache
            if (selector.length > 0) {
              items = filterItems(selector)
            }
            reload($container, items)
          })
        })

      hashFilter($container)
      $(options.filtersGroupSelector).children().removeClass('selected')
      $(
        '.filters li[data-filter=' + window.location.hash.replace('#', '') + ']'
      ).addClass('selected')
    }

    return this.each(function () {
      let $$ = $(this)
      init($$)
      options.selectorType === 'list' ? procUL($$) : proc($$)
    })
  }
})(window.jQuery)
